/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.twentyonegames.slashy.player;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl;

/**
 *
 * @author Marvin
 */
public class PlayerState extends AbstractAppState {
    
    private SimpleApplication app;
    private Camera cam;
    private CameraNode followCam;
    
    private AppStateManager appStateManage;
    private AssetManager assetManager;
    private InputManager inputManager;
    
    private Node rootNode;
    private Node guiNode;
    
    private Node player;
    
    private Vector3f playerMoveTarget;
    
    private final MouseButtonTrigger TRIGGER_MOVE = new MouseButtonTrigger(MouseInput.BUTTON_LEFT);
    private final String MAPPING_MOVE = "Player Move";
    
    private final MouseButtonTrigger TRIGGER_ATTACK = new MouseButtonTrigger(MouseInput.BUTTON_RIGHT);
    private final String MAPPING_ATTACK = "Player Attack";
    
    private ActionListener actionListener = new ActionListener() {

        public void onAction(String name, boolean isPressed, float tpf) {
            if(name.equals(MAPPING_MOVE) && !isPressed) {
                CollisionResults results = new CollisionResults();
                
                Vector2f clickPos = inputManager.getCursorPosition();
                
                //System.out.println("X: " + clickPos.getX() 
                //        + " Y: " + clickPos.getY());
                
                Vector3f worldPos = cam.getWorldCoordinates(
                        new Vector2f(clickPos.getX(), clickPos.getY()), 0f);
                
                Vector3f dir = cam.getWorldCoordinates(
                        new Vector2f(clickPos.getX(), clickPos.getY()), 1f).
                        subtract(worldPos);
                
                Ray ray = new Ray(worldPos, dir);
                
                rootNode.collideWith(ray, results);
                                
                if(results.size() > 0) {
                    //Geometry target = results.getClosestCollision().getGeometry();
                    //System.out.println(target.getName());
                    
                    playerMoveTarget = new Vector3f(
                            results.getClosestCollision().getContactPoint().getX(),
                            3.8f,
                            results.getClosestCollision().getContactPoint().getZ());
                } else {
                    System.out.println("Nothing");
                }
            }
            
            if(name.equals(MAPPING_ATTACK) && isPressed) {
                player.getControl(PlayerControl.class).setAttackAnimation(true);
            }
        }
    };
    
    public PlayerState() {
        
    }
    
    @Override
    public void initialize(AppStateManager manager, Application app) {
        super.initialize(manager, app);
        this.app = (SimpleApplication) app;
        this.appStateManage = manager;
        this.assetManager = this.app.getAssetManager();
        this.inputManager = this.app.getInputManager();
        
        this.followCam = new CameraNode("Follow Cam", this.app.getCamera());
        this.followCam.setControlDir(CameraControl.ControlDirection.SpatialToCamera);
        
        this.cam = this.followCam.getCamera();
        
        this.rootNode = this.app.getRootNode();
        this.guiNode = this.app.getGuiNode();
        
        this.playerMoveTarget = new Vector3f(0f, 3.8f, 0f);
                
        player = (Node)assetManager.loadModel("Models/Player/Player.mesh.j3o");
        
        
        /*AnimControl control;
        control = player.getControl(AnimControl.class);
        for (String anim : control.getAnimationNames()){ System.out.println(anim); }*/
        
        player.setLocalTranslation(0f, 3.8f, 0f);
        Quaternion roll90 = new Quaternion().fromAngleAxis(FastMath.DEG_TO_RAD*-90, Vector3f.UNIT_X);
        player.setLocalRotation(roll90);
        
        followCam.setLocalTranslation(new Vector3f(0f, -15f, 25f));
        followCam.lookAt(player.getLocalTranslation(), Vector3f.UNIT_Y);
                
        player.attachChild(this.followCam);
        
        rootNode.attachChild(player);
        
        player.addControl(new PlayerControl());
        player.getControl(PlayerControl.class).init();
        
        this.inputManager.addMapping(MAPPING_MOVE, TRIGGER_MOVE);
        this.inputManager.addMapping(MAPPING_ATTACK, TRIGGER_ATTACK);
        
        this.inputManager.addListener(actionListener, new String[]{MAPPING_MOVE
            ,MAPPING_ATTACK});
    }
    
    @Override
    public void update(float tpf) {
        if(player.getLocalTranslation().distance(playerMoveTarget) > 2.5f) {
            player.setLocalTranslation(playerMoveTarget);
            player.getControl(PlayerControl.class).setWalkAnimation(true);
        } else {
            player.getControl(PlayerControl.class).setWalkAnimation(false);
        }
    }
    
    @Override
    public void cleanup() {
        
    }
}