/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.twentyonegames.slashy.player;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class PlayerControl extends AbstractControl{

    private boolean isWalking = false;
    
    private boolean isAttacking = false;
    
    private AnimControl control;
    private AnimChannel walk_channel;
     private AnimChannel attack_channel;
    /*
     * 
     */
    
    public PlayerControl() {
    }
    
    public void init() {
        
        control = spatial.getControl(AnimControl.class);
        
        walk_channel = control.createChannel();
        
        attack_channel = control.createChannel();
    }
    
    @Override
    protected void controlUpdate(float tpf) {
        if(isAttacking) {
            attack_channel.setSpeed(0.1f);
            attack_channel.setAnim("Attack"); 
        }
        
        if(isWalking) {
            walk_channel.setLoopMode(LoopMode.DontLoop);
            walk_channel.setAnim("Walk"); 
        } else {
            
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setWalkAnimation(boolean isWalking) {
        this.isWalking = isWalking;
    }
    
    public void setAttackAnimation(boolean isAttacking) {
        this.isAttacking = isAttacking;
    }
    
}
