/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.twentyonegames.slashy;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import eu.twentyonegames.slashy.player.PlayerState;

/**
 *
 * @author Marvin
 */
public class Slashy extends SimpleApplication{
    public static void main(String[] args) {
        AppSettings settings = new AppSettings(true);
        settings.setTitle("Slashy");
        settings.setFullscreen(true);
        settings.setVSync(true);
        
        settings.setResolution(1280, 720);
        settings.setFullscreen(false);
        
        Slashy app = new Slashy();
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        flyCam.setEnabled(false);
        /*flyCam.setEnabled(true);
        flyCam.setMoveSpeed(15f);
        flyCam.setDragToRotate(true);
        cam.setLocation(new Vector3f(0f, 15f, 15f));*/
        
        AmbientLight sun = new AmbientLight();
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
        
        Box ground = new Box(25, 1, 25);
        Geometry groundGeom = new Geometry("Ground", ground);
        
        groundGeom.setLocalTranslation(0f, 0.0f, 0.0f);
        
        Material groundMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        groundMat.setColor("Color", ColorRGBA.Brown);
        groundGeom.setMaterial(groundMat);
        
        rootNode.attachChild(groundGeom);
        
        inputManager.setCursorVisible(true);
        
        stateManager.attach(new PlayerState());
    }
    
    @Override
    public void simpleUpdate(float tpf) {
        
    }
    
    @Override
    public void simpleRender(RenderManager renderer) {
        
    }
    
    /*
     * @Override
     * public void stop() {
     *  
     * }
     */
}
